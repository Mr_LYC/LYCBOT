# LYCBot
`LYCBot`是一个运行在`Google App Engine`的`XMPP`聊天机器人，其基本功能是执行发来的`Python`代码。

## 如何使用
首先请用`Google Talk`添加帐号`lycbot@appspot.com`。  

### 执行表达式：表达式为`#`开头且单行可直接计算的算式。
直接发送表达式，如：
```
#sum(xrange(101))
```

返回的结果为`0 - 100`的和：
> YCPY 2014-03-13 By LYC.  
> ===== result =====  
> 5050  
> ===== end =====  

### 执行代码块
直接发送代码块，如：
```
import math
r = math.pow(5,10)
print r
```
输出结果如下：
> YCPY 2014-03-13 By LYC.  
> ===== out =====  
> 9765625.0  
>   
> ===== end =====  

### LYCBot回复简介
`LYCBot`会回复4部分内容：

 1. `LYCBot Header`：位于头部，表示机器人信息。
 2. `result`：表达式执行结果。
 3. `out`：`stdout`中的内容。
 4. `error`：`stderr`中的内容。

## To do

 1. 支持`stdin`。

 
---
> [刘奕聪](http://mrlyc.blogspot.com/ "我是刘奕聪")  
> [saber000@vip.qq.com](mailto:saber000@vip.qq.com "联系我")