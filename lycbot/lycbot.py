#coding:utf-8
from google.appengine.api import xmpp
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
import ycpy
import platform
import main as entry

class YcPyBot(webapp.RequestHandler):
    def __pack_message(self, **kw):
        kname = ("code", "expression", "result", "out", "error")
        message = '''LYCBot Python Console
YCPY 2014-03-13 By LYC.
Servicing on Google App Engine with Python %s
''' % (platform.python_version(), )
        for k in kname:
            msg = kw.get(k)
            if msg:
                message += '''
===== %s =====
%s
''' % (k, str(msg))

        return message

    def post(self):
        message = xmpp.Message(self.request.POST)


        ycpy_handler = ycpy.YCPY({"run": entry.quick_calc})

        code = message.body.strip()

        reply = "I am LYC."
        exps = [l for l in code.split("\n")]
        exp = str(exps)
        
        result, out, err = ycpy_handler.eval_exp("run(%s)" % exp)
        reply = self.__pack_message(result = result, out = out, error = err)
        message.reply(reply)


app = webapp.WSGIApplication([('/_ah/xmpp/message/chat/', YcPyBot)])

def main():
    run_wsgi_app(app)

if __name__ == "__main__":
    main()